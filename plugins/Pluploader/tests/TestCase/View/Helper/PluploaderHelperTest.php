<?php
namespace Pluploader\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Pluploader\View\Helper\PluploaderHelper;

/**
 * Pluploader\View\Helper\PluploaderHelper Test Case
 */
class PluploaderHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Pluploader\View\Helper\PluploaderHelper
     */
    public $Pluploader;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Pluploader = new PluploaderHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pluploader);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->assertTextContains('plupload.full.min.js', $this->Pluploader->displayAssets());
    }
}
