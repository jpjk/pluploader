<?php
namespace Pluploader\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Pluploader\Model\Table\UploadedFilesTable;

/**
 * Pluploader\Model\Table\UploadedFilesTable Test Case
 */
class UploadedFilesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Pluploader\Model\Table\UploadedFilesTable
     */
    public $UploadedFiles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.pluploader.uploaded_files',
        'plugin.pluploader.objects'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UploadedFiles') ? [] : ['className' => UploadedFilesTable::class];
        $this->UploadedFiles = TableRegistry::get('UploadedFiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UploadedFiles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
