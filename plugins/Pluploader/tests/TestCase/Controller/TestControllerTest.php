<?php
namespace Pluploader\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Pluploader\Controller\TestController;

/**
 * Pluploader\Controller\TestController Test Case
 */
class TestControllerTest extends IntegrationTestCase
{
    /**
     * Test performTest method
     *
     * @return void
     */
    public function testRouteIsReachable()
    {
        $this->get('/pluploader/test');
        $this->assertResponseOk();
    }

    public function testRouteHasRequiredAssets()
    {
        $this->get('/pluploader/test');
        $this->assertResponseContains('/pluploader/js/plupload.full.min.js');
    }
}
