<?php
namespace Pluploader\Model\Entity;

use Cake\ORM\Entity;

/**
 * UploadedFile Entity
 *
 * @property int $id
 * @property string $name
 * @property string $original_name
 * @property string $extension
 * @property string $object_name
 * @property int $object_id
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \Pluploader\Model\Entity\Object $object
 */
class UploadedFile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'original_name' => true,
        'extension' => true,
        'object_name' => true,
        'object_id' => true,
        'created' => true,
        'object' => true
    ];
}
