<?php
namespace Pluploader\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UploadedFiles Model
 *
 * @property \Pluploader\Model\Table\ObjectsTable|\Cake\ORM\Association\BelongsTo $Objects
 *
 * @method \Pluploader\Model\Entity\UploadedFile get($primaryKey, $options = [])
 * @method \Pluploader\Model\Entity\UploadedFile newEntity($data = null, array $options = [])
 * @method \Pluploader\Model\Entity\UploadedFile[] newEntities(array $data, array $options = [])
 * @method \Pluploader\Model\Entity\UploadedFile|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Pluploader\Model\Entity\UploadedFile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Pluploader\Model\Entity\UploadedFile[] patchEntities($entities, array $data, array $options = [])
 * @method \Pluploader\Model\Entity\UploadedFile findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UploadedFilesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('uploaded_files');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('original_name')
            ->requirePresence('original_name', 'create')
            ->notEmpty('original_name');

        $validator
            ->scalar('extension')
            ->requirePresence('extension', 'create')
            ->notEmpty('extension');

        $validator
            ->scalar('object_name')
            ->requirePresence('object_name', 'create')
            ->notEmpty('object_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }
}
