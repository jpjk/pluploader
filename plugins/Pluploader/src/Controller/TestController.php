<?php

namespace Pluploader\Controller;

use Cake\Controller\Controller;

class TestController extends Controller
{
    public function performTest()
    {

    }

    public function testUpload()
    {
        $this->loadComponent('Pluploader.Pluploader');
        $this->Pluploader->handleUpload('TestEntity');
    }
}