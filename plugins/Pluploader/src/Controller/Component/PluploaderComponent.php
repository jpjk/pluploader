<?php

namespace Pluploader\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Symfony\Component\Filesystem\Filesystem;

class PluploaderComponent extends Component
{
    /** @var array $pluploader_config */
    private $pluploader_config = [];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setPluploaderConfig($this->readConfig());
    }

    /**
     * @param string $entity_name
     */
    public function handleUpload($entity_name)
    {
        $directory_path = $this->getDirectoryPath($entity_name);

        $this->upload($directory_path);
    }

    public function upload($targetDir)
    {
        set_time_limit(5 * 60);

        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds
        $fs = new Filesystem();

        if (!$fs->exists($targetDir)) {
            $fs->mkdir($targetDir);
        }
        $name = $this->getController()->request->getData('name');
        $extension = $this->getFileExtension($name);
        $fileName = preg_replace('/[^\w._]+/', '_', $name) . '.' . $extension;

        $filePath = $targetDir . DS . $fileName;

        // Chunking might be enabled
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        // Remove old temp files
        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DS . $file;

                // If temp file is current file proceed to the next
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }

                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }


        // Open temp file
        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
            }

            // Read binary input stream and append it to temp file
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);
        }

        // Return Success JSON-RPC response
        die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');

    }

    /**
     * @param string $entity_name
     * @return string
     */
    public function getDirectoryPath($entity_name)
    {
        $entity_name = strtolower(preg_replace('/\s+/', '', $entity_name));
        $config = $this->getPluploaderConfig();

        return WWW_ROOT
            . $config['default_directory']
            . DS
            . $entity_name
            . DS;
    }

    /**
     * @return array
     */
    public function readConfig()
    {
        $defaults = [
            'default_directory' => 'uploads'
        ];

        foreach ($defaults as $index => $default) {
            if ($setting = Configure::read('Pluploader.' . $index)) {
                $defaults[$index] = $setting;
            }
        }

        return $defaults;
    }

    /**
     * @return array
     */
    public function getPluploaderConfig(): array
    {
        return $this->pluploader_config;
    }

    /**
     * @param array $pluploader_config
     */
    public function setPluploaderConfig(array $pluploader_config)
    {
        $this->pluploader_config = $pluploader_config;
    }

    /**
     * @param $file_name
     * @return mixed
     */
    public function getFileExtension($file_name)
    {
        $exploded = explode('.', $file_name);
        return end($exploded);
    }
}