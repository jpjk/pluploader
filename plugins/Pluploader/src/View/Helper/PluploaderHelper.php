<?php
namespace Pluploader\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Pluploader helper
 *
 * @property \Cake\View\Helper\HtmlHelper $Html
 */
class PluploaderHelper extends Helper
{
    public $helpers = ['Html'];

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function displayAssets()
    {
        $view = new View();
        return $view->element('Pluploader.assets');
    }
}
