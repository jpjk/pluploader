<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::plugin(
    'Pluploader',
    ['path' => '/pluploader'],
    function (RouteBuilder $routes) {
        $routes->connect(
            '/test-upload', [
                'controller' => 'Test',
                'action'     => 'testUpload',
            ]
        );

        $routes->connect(
            '/test', [
                'controller' => 'Test',
                'action'     => 'performTest',
            ]
        );
    }
);
